Live streaming via ffmpeg
=========================

## Usage

    $ brew install ffmpeg --with-fdk-aac
    $ ffmpeg -hide_banner -f avfoundation -list_devices true -i ""
    $ ./start.sh -v "Logitec" -a 0 -b 3000 -u "rmtp://live..."

If streaming is too slow try a lower value eg -b 2000 or b -1500. Below is the ffmpeg command run by the script:

    $ ffmpeg -f avfoundation -thread_queue_size 512 -video_size 1280x720 -framerate 30 -i "VIDEO:AUDIO" -acodec copy "raw $(date).wav" -i assets/logocrop.png -filter_complex "overlay=main_w-overlay_w-10:main_h-overlay_h-10[logo]" -f flv -map "[logo]" -map 0:a -ac 2 -ar 44100 -vcodec libx264 -g 60 -keyint_min 30 -b:a 128k -minrate 3000k -maxrate 3000k -pix_fmt yuv420p -s 1280x720 -preset ultrafast -tune film -acodec libfdk_aac -threads 0 -strict normal -bufsize 3000k "rtmp://"

## Notes

On broken pipe errors, press up and run the same command again.
