#!/bin/bash

usage() { echo "Usage: $0 [-v 0] [-a 0] [-b 3000] [-u <Stream or Server URL from www.facebook.com/nagoyabuzz/publishing_tools/>] " 1>&2; exit 1; }

while getopts "v:a:b:u: " o; do
    case "${o}" in

        v)
            v=${OPTARG}
            ;;
        a)
            a=${OPTARG}
            ;;
        b)
            b=${OPTARG}
            ;;
        u)     
            u=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${v}" ] || [ -z "${a}" ] || [ -z "${b}" ] || [ -z "${u}" ]; then
    usage
fi

ffmpeg -hide_banner -f avfoundation -thread_queue_size 512 -video_size 1280x720 -i "${v}:${a}" -acodec copy "raw $(date).wav" -i assets/logocrop.png -filter_complex "overlay=main_w-overlay_w-10:main_h-overlay_h-10[logo]" -f flv -map "[logo]" -map 0:a -ac 2 -ar 44100 -vcodec libx264 -g 60 -keyint_min 30 -b:a 128k -minrate ${b}k -maxrate ${b}k -pix_fmt yuv420p -s 1280x720 -preset ultrafast -tune film -acodec libfdk_aac -threads 0 -strict normal -bufsize ${b}k "rtmp://live-api.facebook.com:80/rtmp/${u}"